import styles from "./product-price-information.module.scss";

const PriceInformation = ({ price, offers, guarantee }) => {
  return (
    <div className={styles.priceInformation}>
      <div className={styles.price}>&pound;{price}</div>
      <div className={styles.offers}>{offers}</div>
      <div className={styles.guarantee}>{guarantee}</div>
    </div>
  );
};

export default PriceInformation;
