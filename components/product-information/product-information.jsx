import styles from "./product-information.module.scss";

const PriceInformation = ({ copy }) => {
  return (
    <div className={styles.productInformation}>
      <h3>Product information</h3>
      <div dangerouslySetInnerHTML={{ __html: copy }}></div>
    </div>
  );
};

export default PriceInformation;
