# Quality Engineer UI Technical Test

## Context

This exercise is part of the recruitment process for Quality Engineers for the John Lewis Partnership.

Once you have applied for a Quality Engineering role, you are asked to do this exercise as part of the selection process.

This exercise fits into the recruitment process as follows:

1. Complete the application form and supply your CV. (If you are reading this you will have already done that.)
2. Complete the pre-interview exercises which include:
    1. An exercise in critical thinking and continuous testing.
    2. An exercise in displaying knowledge of exploratory testing.
    3. An exercise in writing either
        1. EITHER simple UI automation (<-- the exercise in this repo)
        2. OR simple API test automation.


## Brief

This task asks you to create some simple UI automation tests for the Dishwasher App. This App allows customers to see the range of dishwashers John Lewis sells.

The App has a Product Grid Page and Product Details Pages. When the website is launched, the customer will be presented with a grid of dishwashers that are currently available for customers to buy. When a dishwasher is clicked, a new screen is displayed showing the dishwasher’s details.

Please follow the instructions below to create your own fork of this repository and a local running copy of the App. Then please create some simple automated tests. Store the code for these tests in your fork of the repository.

You may use an automation tool/framework of your choice.

## Expectations

- You will give access to your fork of the repository to a small group of John Lewis Partnership Partners whose names and email addresses will already have been provided to you.
- Your fork of the repository should have added to it the code you have authored to run your tests.
- You should be able to describe the tests and the risks that they address. Should you progress to the next stage of the recruitment process you may be asked about this.
- We believe you should aim for a maximum of 5 tests. We appreciate that this will involve some investment of your time so if you only have time to get one working test then that will be fine.


---

## Getting the App running locally

1. Fork this repo into your own GitLab namespace. (You will need a GitLab account.)
2. Clone the project to your local machine. (You will need to be familiar with Git commands executed locally.)
3. Make the directory created by the clone operation the current working directory
4. Install the NPM dependencies using `npm i`. (You will need to be on node.js `v19.x`). You can check by running `node --version`. We recommend using [https://github.com/nvm-sh/nvm](Node Version Manager) to manage your node.js version
5. Run the development server with `npm run dev`
6. You can check that the server is running by navigating to [http://localhost:3000](http://localhost:3000).

