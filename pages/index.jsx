import Head from "next/head";
import Link from "next/link";
import styles from "./index.module.scss";
import ProductListItem from "../components/product-list-item/product-list-item";
import data from '../mockData/data.json'

export async function getServerSideProps(context) {
  return {
    props: { data },
  };
}

const Home = ({ data = {}}) => {
  let items = data.products || {};
  return (
    <div>
      <Head>
        <title>JL &amp; Partners | Home</title>
        <meta name="keywords" content="shopping" />
      </Head>
      <div>
        <h1>Dishwashers ({data.products?.length})</h1>
        <div className={styles.content}>
          {items?.map?.((item, index) => (
            <Link
              key={`${item.productId}-${item.title}`}
              href={{
                pathname: "/product-detail/[id]",
                query: { id: index === 4 ? `${item.productId}-x`: item.productId  },
              }}
              className={styles.link}>

                <ProductListItem
                  image={item.image}
                  description={item.title}
                  price={item.price.now}
                />

            </Link>
          ))}
        </div>
      </div>
    </div>
  );
};

export default Home;
