import ProductCarousel from "../../components/product-carousel/product-carousel";
import PriceInformation from "../../components/product-price-information/product-price-information";
// import ProductReadMore from "../../components/product-read-more/product-read-more";
import ProductInformation from "../../components/product-information/product-information";
import ProductSpecification from "../../components/product-specification/product-specification";
import productData from '../../mockData/data2.json'

import styles from "./product-detail.module.scss";
import Link from 'next/link';

export async function getServerSideProps(context) {
  const id = context.params.id;
  const data = productData.products.find(product => product.productId === id)
  if (!data) {
    return {
      props: {}
    }
  }

  return {
    props: { data: data },
  };
}

const ProductDetail = ({ data }) => {
  if (!data) {
    return (
      <div className="not-found" style={{margin: '0 auto', width: '90%'}}>
        <h1>Oh dear...</h1>
        <h2>That product doesn't seem to exist</h2>
        <p>
          Go back to the{" "}
          <Link href="/">
            Homepage
          </Link>
        </p>
      </div>
    )
  }
  return (
    <div>
      <h1 className={styles.h1}>{data.title}</h1>
      <div className={styles.content}>
        <div className={styles.productCarousel}>
          <ProductCarousel image={data.media.images.urls[0]} />
        </div>
        <div className={styles.priceInformation}>
          <PriceInformation
            price={data.price.now}
            offers={data.displaySpecialOffer}
            guarantee={data.additionalServices.includedServices}
          />
        </div>
        <div className={styles.productInformation}>
          <ProductInformation copy={data.details.productInformation} />
        </div>
        {/* <div className={styles.readMore}>
          <ProductReadMore copy={{ title: "read more.." }} />
        </div> */}
        <div className={styles.productSpecification}>
          <ProductSpecification
            specifications={data.details.features[0].attributes}
          />
        </div>
      </div>
    </div>
  );
};

export default ProductDetail;
